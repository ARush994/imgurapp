package com.rush.imgurapp.models;

import android.graphics.Bitmap;

import androidx.annotation.NonNull;
import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.rush.imgurapp.BR;

@Entity
public class PostModel extends BaseObservable {
    @NonNull
    @PrimaryKey
    @SerializedName("id")
    @Expose
    String id;

    @SerializedName("title")
    @Expose
    String title;
    @SerializedName("datetime")
    @Expose
    long datetime;
    @SerializedName("cover")
    @Expose
    String cover;
    @SerializedName("account_url")
    @Expose
    String owner;
    @SerializedName("views")
    @Expose
    int views;
    @SerializedName("link")
    @Expose
    String linkToPost;
    @SerializedName("ups")
    @Expose
    int thumbUps;
    @SerializedName("downs")
    @Expose
    int thumbDowns;
    @SerializedName("comment_count")
    @Expose
    int commentCount;
    @SerializedName("is_album")
    @Expose
    boolean isAlbum;

    public String getId() {
        return id;
    }
    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }
    public void setTitle(String title) {
        this.title = title;
    }

    public long getDatetime() {
        return datetime;
    }
    public void setDatetime(long datetime) {
        this.datetime = datetime;
    }

    public String getCover() {
        return cover;
    }
    public void setCover(String cover) {
        this.cover = cover;
    }

    public String getOwner() {
        return owner;
    }
    public void setOwner(String owner) {
        this.owner = owner;
    }

    public int getViews() {
        return views;
    }
    public void setViews(int views) {
        this.views = views;
    }

    public String getLinkToPost() {
        return linkToPost;
    }
    public void setLinkToPost(String linkToPost) {
        this.linkToPost = linkToPost;
    }

    public int getThumbUps() {
        return thumbUps;
    }
    public void setThumbUps(int thumbUps) {
        this.thumbUps = thumbUps;
    }

    public int getThumbDowns() {
        return thumbDowns;
    }
    public void setThumbDowns(int thumbDowns) {
        this.thumbDowns = thumbDowns;
    }

    public int getCommentCount() {
        return commentCount;
    }
    public void setCommentCount(int commentCount) {
        this.commentCount = commentCount;
    }

    public boolean getIsAlbum() {
        return isAlbum;
    }
    public void setIsAlbum(boolean album) {
        isAlbum = album;
    }
}
