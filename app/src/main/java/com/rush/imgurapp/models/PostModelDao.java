package com.rush.imgurapp.models;

import androidx.room.Dao;
import androidx.room.Insert;

import java.util.List;

@Dao
public interface PostModelDao {
    @Insert
    void insertAll(List<PostModel> posts);
}
