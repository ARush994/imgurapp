package com.rush.imgurapp.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class CommentModel {
    @SerializedName("id")
    @Expose
    private long id;
    @SerializedName("image_id")
    @Expose
    private String image_id;
    @SerializedName("comment")
    @Expose
    private String comment;
    @SerializedName("author")
    @Expose
    private String author;
    @SerializedName("ups")
    @Expose
    private int ups;
    @SerializedName("platform")
    @Expose
    private String platform;

    private boolean api = false;
    public boolean isApi() {
        return api;
    }
    public void setApi(boolean api) {
        this.api = api;
    }

    private boolean android = false;
    public boolean isAndroid() {
        return android;
    }
    public void setAndroid(boolean android) {
        this.android = android;
    }

    private boolean apple = false;
    public boolean isApple() {
        return apple;
    }
    public void setApple(boolean apple) {
        this.apple = apple;
    }

    private boolean desktop = false;
    public boolean isDesktop() {
        return desktop;
    }
    public void setDesktop(boolean desktop) {
        this.desktop = desktop;
    }

    private boolean text = false;
    public boolean isText() {
        return text;
    }
    public void setText(boolean text) {
        this.text = text;
    }

    public long getId() {
        return id;
    }
    public void setId(long id) {
        this.id = id;
    }

    public String getImage_id() {
        return image_id;
    }
    public void setImage_id(String image_id) {
        this.image_id = image_id;
    }

    public String getComment() {
        return comment;
    }
    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getAuthor() {
        return author;
    }
    public void setAuthor(String author) {
        this.author = author;
    }

    public int getUps() {
        return ups;
    }
    public void setUps(int ups) {
        this.ups = ups;
    }

    public String getPlatform() {
        return platform;
    }
    public void setPlatform(String platform) {
        this.platform = platform;
    }
}
