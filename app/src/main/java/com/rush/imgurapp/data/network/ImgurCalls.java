package com.rush.imgurapp.data.network;

import androidx.fragment.app.FragmentActivity;
import androidx.recyclerview.widget.RecyclerView;

import com.rush.imgurapp.adapters.listeners.OnPhotoListener;
import com.rush.imgurapp.models.ResponseWithPostModel;
import com.rush.imgurapp.models.ResponseWithPostsModel;
import com.rush.imgurapp.viewmodels.MainViewModel;

import io.reactivex.annotations.NonNull;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ImgurCalls {
    private static ImgurCalls instance;
    public static ImgurCalls getInstance() {
        if (instance == null) {
            instance = new ImgurCalls();
        }
        return instance;
    }

    private ImgurCalls() {

    }

    public ImgurService getImgurService() {
        return ImgurService.retrofit.create(ImgurService.class);
    }

    public void getPostsList(FragmentActivity fragment,
                               OnPhotoListener listener,
                               MainViewModel viewModel,
                               RecyclerView rv) {
        getImgurService()
                .getPhotos(1)
                .enqueue(new Callback<ResponseWithPostsModel>() {
                    @Override
                    public void onResponse(Call<ResponseWithPostsModel> call, Response<ResponseWithPostsModel> response) {
                        ResponseWithPostsModel responseModel = response.body();
                        viewModel.setPostsList(responseModel.getData());

                        fragment.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                viewModel.renderPostsList(listener, rv, false);
                            }
                        });
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseWithPostsModel> call, @NonNull Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    public void getMediaItem(FragmentActivity fragment,
                             MainViewModel viewModel) {
        getImgurService()
                .getImageInfo(viewModel.getCurrentPostId())
                .enqueue(new Callback<ResponseWithPostModel>() {
                    @Override
                    public void onResponse(Call<ResponseWithPostModel> call, Response<ResponseWithPostModel> response) {
                        ResponseWithPostModel responseModel = response.body();
                        viewModel.setPostInner(responseModel.getData());

                        fragment.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                viewModel.selectFragment();
                            }
                        });
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseWithPostModel> call, @NonNull Throwable t) {
                        t.printStackTrace();
                    }
                });
    }

    public void getCommentsList(FragmentActivity fragment,
                             MainViewModel viewModel) {
        getImgurService()
                .getImageInfo(viewModel.getCurrentPostId())
                .enqueue(new Callback<ResponseWithPostModel>() {
                    @Override
                    public void onResponse(Call<ResponseWithPostModel> call, Response<ResponseWithPostModel> response) {
                        ResponseWithPostModel responseModel = response.body();
                        viewModel.setPostInner(responseModel.getData());

                        fragment.runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                viewModel.selectFragment();
                            }
                        });
                    }

                    @Override
                    public void onFailure(@NonNull Call<ResponseWithPostModel> call, @NonNull Throwable t) {
                        t.printStackTrace();
                    }
                });
    }
}
