package com.rush.imgurapp.data.database;

import androidx.room.Room;

import com.rush.imgurapp.App;
import com.rush.imgurapp.R;

public class DatabaseManager {
    private static DatabaseManager instance;
    public static DatabaseManager getInstance()
    {
        if (instance == null)
        {
            instance = new DatabaseManager();
        }
        return instance;
    }

    private AppDatabase database;
    private DatabaseManager()
    {
        database = Room.databaseBuilder(App.getInstance().getBaseContext(), AppDatabase.class,
                String.format("%s database", R.string.app_name)).build();
    }
}
