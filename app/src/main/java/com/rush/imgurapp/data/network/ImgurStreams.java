package com.rush.imgurapp.data.network;

import com.rush.imgurapp.models.ResponseWithCommentModel;
import com.rush.imgurapp.models.ResponseWithPostModel;
import com.rush.imgurapp.models.ResponseWithPostsModel;
import com.rush.imgurapp.viewmodels.MainViewModel;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.Single;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.functions.Function;
import io.reactivex.schedulers.Schedulers;

public class ImgurStreams {

    public static Observable<ResponseWithPostsModel> streamFetchPosts(int lastLoadedPage) {
        ImgurService imgurService = ImgurService.retrofit.create(ImgurService.class);
        return imgurService.getPostsReactive(lastLoadedPage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .timeout(10, TimeUnit.SECONDS);
    }

    public static Single<ResponseWithPostsModel> streamFetchPostsSingle(int lastLoadedPage) {
        ImgurService imgurService = ImgurService.retrofit.create(ImgurService.class);
        return imgurService.getPostsSingleReactive(lastLoadedPage)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .timeout(10, TimeUnit.SECONDS);
    }

    // run 1
    public static Observable<ResponseWithPostModel> streamFetchPostData(String currentMediaId) {
        ImgurService imgurService = ImgurService.retrofit.create(ImgurService.class);
        return imgurService.getPostInfoReactive(currentMediaId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .timeout(10, TimeUnit.SECONDS);
    }

    // run 3
    public static Observable<ResponseWithCommentModel> streamFetchComments(String currentMediaId){
        ImgurService gitHubService = ImgurService.retrofit.create(ImgurService.class);
        return gitHubService.getPostCommentsReactive(currentMediaId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .timeout(10, TimeUnit.SECONDS);
    }

    public static Observable<ResponseWithCommentModel> streamFetchPostAndComments(String currentMediaId, MainViewModel viewModel) {
        return streamFetchPostData(currentMediaId)
                .map(new Function<ResponseWithPostModel, ResponseWithPostModel>() { // run 2
                    @Override
                    public ResponseWithPostModel apply(ResponseWithPostModel response) throws Exception {
                        viewModel.setPostInner(response.getData());
                        viewModel.selectFragment();
                        return response;
                    }
                })
                .flatMap(new Function<ResponseWithPostModel, Observable<ResponseWithCommentModel>>() {
                    @Override
                    public Observable<ResponseWithCommentModel> apply(ResponseWithPostModel response) throws Exception {
                        return streamFetchComments(viewModel.getCurrentPostId()); // "I57tpA2"
                    }
                });
    }
}
