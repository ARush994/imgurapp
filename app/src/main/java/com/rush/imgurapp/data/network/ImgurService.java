package com.rush.imgurapp.data.network;

import com.rush.imgurapp.models.ResponseWithCommentModel;
import com.rush.imgurapp.models.ResponseWithPostModel;
import com.rush.imgurapp.models.ResponseWithPostsModel;

import io.reactivex.Observable;
import io.reactivex.Single;
import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Path;

public interface ImgurService {
    /////////////////////////////////////////////////////
    // GET request to get the list of required posts
    @Headers({
            "Authorization: Client-ID 046823c07ef6628"
    })
    @GET("3/gallery/top/{page}?showViral=true")
    Call<ResponseWithPostsModel> getPhotos(@Path("page") int page);
    //////////////////////////////////////////////////////
    // GET request to get info about single post
    @Headers({
            "Authorization: Client-ID 046823c07ef6628"
    })
    @GET("3/image/{imageHash}")
    Call<ResponseWithPostModel> getImageInfo(@Path("imageHash") String imageHash);
    //////////////////////////////////////////////////////



    // GET request to get the list of required posts
    @Headers({
            "Authorization: Client-ID 046823c07ef6628"
    })
    @GET("3/gallery/top/{page}?showViral=true")
    //Single<Observable<ResponseWithPostsModel>> getPostsReactive(@Path("page") int page);
    Observable<ResponseWithPostsModel> getPostsReactive(@Path("page") int page);

    // GET request to get the list of required posts
    @Headers({
            "Authorization: Client-ID 046823c07ef6628"
    })
    @GET("3/gallery/top/{page}?showViral=true")
    //Single<Observable<ResponseWithPostsModel>> getPostsReactive(@Path("page") int page);
    Single<ResponseWithPostsModel> getPostsSingleReactive(@Path("page") int page);
    // GET request to get info about single post
    @Headers({
            "Authorization: Client-ID 046823c07ef6628"
    })
    @GET("3/image/{imageHash}")
    Observable<ResponseWithPostModel> getPostInfoReactive(@Path("imageHash") String imageHash);
    //////////////////////////////////////////////////
    // GET request to get info about single post
    @Headers({
            "Authorization: Client-ID 046823c07ef6628"
    })
    @GET("3/gallery/{imageHash}/comments/best")
    Observable<ResponseWithCommentModel> getPostCommentsReactive(@Path("imageHash") String imageHash);
    //////////////////////////////////////////////////

    String BASE_URL = "https://api.imgur.com/";

    Retrofit retrofit = new Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .build();
}
