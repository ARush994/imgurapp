package com.rush.imgurapp.data.network;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.recyclerview.widget.RecyclerView;

import com.rush.imgurapp.App;
import com.rush.imgurapp.R;
import com.rush.imgurapp.databinding.MainFragmentSingleViewBinding;
import com.rush.imgurapp.models.ResponseWithCommentModel;
import com.rush.imgurapp.viewmodels.MainViewModel;
import com.squareup.picasso.Picasso;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.schedulers.Schedulers;

public class NetworkCalls {

    private static NetworkCalls instance;
    public static NetworkCalls getInstance() {
        if (instance == null) {
            instance = new NetworkCalls();
        }
        return instance;
    }

    //region SingleFragment: downloadMedia
    public void downloadMedia(){

    }

    /**
     * Async Task to download file from URL
     */
    public static final String TAG = "Activity info: ";
    public static class MediaDownloading extends AsyncTask<String, String, String> {

        private ProgressDialog progressDialog;
        private String fileName;
        private String folder;
        private String file;
        private boolean isDownloaded;
        private Context context;

        public MediaDownloading(Context context)
        {
            this.context = context;
        }
        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = new ProgressDialog(context);
            this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            this.progressDialog.setCancelable(false);
            this.progressDialog.show();
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();
                // getting file length
                int lengthOfFile = connection.getContentLength();


                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

                //Extract file name from URL
                fileName = f_url[0].substring(f_url[0].lastIndexOf('/') + 1, f_url[0].length());

                //Append timestamp to file name
                fileName = timestamp + "_" + fileName;

                //External directory path to save file
                folder = Environment.getExternalStorageDirectory() + File.separator +
                        App.getInstance().getResources().getString(R.string.app_name)
                        + "DownloadedMedia/";
                //Create folder if it does not exist
                File directory = new File(folder);
                if (!directory.exists()) {
                    directory.mkdirs();
                }

                file = Environment.getExternalStorageDirectory() + File.separator +
                        App.getInstance().getResources().getString(R.string.app_name)
                        + "DownloadedMedia/" + fileName;

                // Output stream to write file
                FileOutputStream fos = new FileOutputStream(file);
                //DataOutputStream output = new DataOutputStream(fos);
                OutputStream output = new FileOutputStream(file);


                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));
                    Log.d(TAG, "Progress: " + (int) ((total * 100) / lengthOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();
                return "Downloaded at: " + folder + fileName;

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return "Something went wrong";
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            progressDialog.setProgress(Integer.parseInt(progress[0]));
        }


        @Override
        protected void onPostExecute(String message) {
            // dismiss the dialog after the file was downloaded
            this.progressDialog.dismiss();

            // Display File path after downloading
            Toast.makeText(context,
                    message, Toast.LENGTH_LONG).show();
        }
    }
    //endregion

    //region SingleFragmet: HTTP (RxJAVA)
    private Disposable disposable;
    // Execute Stream
    public void httpGettingMediaModel(MainViewModel viewModel, MainFragmentSingleViewBinding bindable, RecyclerView mRecyclerView){
        // Execute the stream subscribing to Observable defined inside ImgurStream
        this.disposable = ImgurStreams.streamFetchPostAndComments(viewModel.getCurrentPostId(), viewModel).subscribeWith(new DisposableObserver<ResponseWithCommentModel>() {
            @Override
            public void onNext(ResponseWithCommentModel response) {
                Log.e(TAG,"On Next");
                // 1.3 - Select fragment that will shown
                viewModel.setCommentsList(response.getData());
                //viewModel.selectFragment();
                bindable.setModel(viewModel);
                viewModel.renderCommentsList(mRecyclerView);
            }

            @Override
            public void onError(Throwable e) {
                Log.e(TAG,"On Error"+Log.getStackTraceString(e));
                bindable.layoutComments.setVisibility(View.VISIBLE);
            }

            @Override
            public void onComplete() {
                Log.e(TAG,"On Complete !!");
            }
        });
    }

    public void destroyingDisposable()
    {
        if (this.disposable != null && !this.disposable.isDisposed()) this.disposable.dispose();
    }

    //endregion

    //region ListFragment: Paging
    private PublishProcessor<Integer> paginator = PublishProcessor.create();

    public PublishProcessor<Integer> getPaginator() {
        return paginator;
    }
    public void setPaginator(PublishProcessor<Integer> paginator) {
        this.paginator = paginator;
    }

    private CompositeDisposable compositeDisposable = new CompositeDisposable();

    public void subscribeingForData(ProgressBar mProgressBar, MainViewModel viewModel) {
        Disposable disposable2 = paginator
                .onBackpressureDrop()
                .doOnNext(page -> {
                    viewModel.setLoading(true);
                    mProgressBar.setVisibility(View.VISIBLE);
                })
                .concatMapSingle(page -> ImgurStreams.streamFetchPostsSingle(page)
                        .subscribeOn(Schedulers.io())
                        .doOnError(throwable -> {
                            Log.d("concatMapSingle", "subscribeForData: ");
                        }))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(items -> {
                    viewModel.addToPostsList(items.getData());
                    viewModel.getPhotosAdapter().addItems(items.getData());
                    viewModel.getPhotosAdapter().notifyDataSetChanged();
                    viewModel.setLoading(false);
                    mProgressBar.setVisibility(View.INVISIBLE);
                });

        compositeDisposable.add(disposable2);

        paginator.onNext(viewModel.getLastLoadedPage());
    }

    public void destroyingCompositeDisposable(){
        compositeDisposable.clear();
    }
    //endregion
}
