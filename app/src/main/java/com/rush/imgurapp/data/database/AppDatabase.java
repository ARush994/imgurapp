package com.rush.imgurapp.data.database;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.rush.imgurapp.models.PostModel;
import com.rush.imgurapp.models.PostModelDao;

@Database(entities = {PostModel.class,}, version = 1)
public abstract class AppDatabase extends RoomDatabase {
    public abstract PostModelDao postModelDao();
}
