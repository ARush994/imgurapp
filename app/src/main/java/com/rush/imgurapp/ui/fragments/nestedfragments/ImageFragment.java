package com.rush.imgurapp.ui.fragments.nestedfragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.rush.imgurapp.R;
import com.rush.imgurapp.databinding.ImageViewBinding;
import com.rush.imgurapp.viewmodels.MainViewModel;

public class ImageFragment extends Fragment {
    ImageViewBinding bindable;

    MainViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        bindable = DataBindingUtil.inflate(inflater, R.layout.image_view, container, false);

        viewModel = MainViewModel.getInstance();

        bindable.setUrl(viewModel.getPostInner().getLink());

        return bindable.getRoot();
    }
}
