package com.rush.imgurapp.ui;

import android.Manifest;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;

import com.rush.imgurapp.R;
import com.rush.imgurapp.data.Constants;
import com.rush.imgurapp.databinding.MainActivityViewBinding;
import com.rush.imgurapp.root.RootActivity;
import com.rush.imgurapp.data.network.ImgurStreams;
import com.rush.imgurapp.utils.NetworkChangeReceiver;
import com.rush.imgurapp.viewmodels.MainViewModel;

import java.util.List;

import javax.inject.Inject;

import pub.devrel.easypermissions.EasyPermissions;

import static com.rush.imgurapp.data.Constants.CONNECTIVITY_ACTION;

public class MainActivity extends RootActivity implements EasyPermissions.PermissionCallbacks {

    private static final int WRITE_REQUEST_CODE = 300;

    IntentFilter intentFilter;
    NetworkChangeReceiver receiver;

    private MainActivityViewBinding bindable;
    @Inject
    MainViewModel viewModel;

    @Inject
    ImgurStreams imgurStreams;

    @Override
    protected void onResume() {
        super.onResume();
        viewModel.initializePlayer();
        registerReceiver(receiver, intentFilter);
        Log.d(Constants.TAG, "MainActivity: onResume()");
    }

    @Override
    protected void onPause() {
        super.onPause();
        unregisterReceiver(receiver);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        bindable = DataBindingUtil.setContentView(this, R.layout.main_activity_view);

        intentFilter = new IntentFilter();
        intentFilter.addAction(CONNECTIVITY_ACTION);
        receiver = new NetworkChangeReceiver();
        receiver.setBindable(bindable);

        //Snackbar
        bindable.setModel(viewModel);
        viewModel.setFragmentManager(getSupportFragmentManager());
    }

    protected void onDestroy(){
        super.onDestroy();
        viewModel.releasePlayer();
        Log.d(Constants.TAG, "MainActivity: onDestroy()");
    }

    public void CopyLinkToClipboard(View view) {
        ClipboardManager clipboard = (ClipboardManager)getSystemService(CLIPBOARD_SERVICE);
        CharSequence label = "copied";
        CharSequence textToCopy = viewModel.getPostInner().getLink();
        ClipData clip = ClipData.newPlainText(label, textToCopy);
        clipboard.setPrimaryClip(clip);
        Toast.makeText(this, label, Toast.LENGTH_SHORT).show();
    }

    public void DownloadMedia(View view) {
        boolean canSave = viewModel.isSDCardPresent();
        if (canSave)
        {
            //check if app has permission to write to the external storage.
            if (EasyPermissions.hasPermissions(MainActivity.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                //Get the URL entered
                viewModel.mediaDownloading(this);

            } else {
                //If permission is not present request for the same.
                EasyPermissions.requestPermissions(MainActivity.this, getString(R.string.write_file), WRITE_REQUEST_CODE, Manifest.permission.READ_EXTERNAL_STORAGE);
            }
        } else {
            Toast.makeText(getApplicationContext(),
                    "SD Card not found", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onBackPressed() {
        if (getSupportFragmentManager().getBackStackEntryCount() > 0){
            viewModel.playVideo(false);
            viewModel.setPostInfoPage(false);
            getSupportFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, MainActivity.this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        //Download the file once permission is granted
        viewModel.mediaDownloading(this);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d(Constants.TAG, "Permission has been denied");
    }
}
