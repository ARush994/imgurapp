package com.rush.imgurapp.ui.fragments.nestedfragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.Fragment;

import com.google.android.exoplayer2.ui.SimpleExoPlayerView;
import com.rush.imgurapp.R;
import com.rush.imgurapp.databinding.ExoplayerViewBinding;
import com.rush.imgurapp.viewmodels.MainViewModel;

public class VideoFragment extends Fragment {

    ExoplayerViewBinding bindable;

    MainViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        bindable = DataBindingUtil.inflate(inflater, R.layout.exoplayer_view, container, false);

        viewModel = MainViewModel.getInstance();

        //Initialize simpleExoPlayerView
        SimpleExoPlayerView simpleExoPlayerView = bindable.exoPlayer;
        simpleExoPlayerView.setPlayer(viewModel.getPlayer());

        viewModel.setPlayerUri();

        return bindable.getRoot();
    }
}
