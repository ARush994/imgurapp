package com.rush.imgurapp.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.rush.imgurapp.R;
import com.rush.imgurapp.adapters.listeners.OnPhotoListener;
import com.rush.imgurapp.databinding.MainFragmentListViewBinding;
import com.rush.imgurapp.root.RootFragment;
import com.rush.imgurapp.viewmodels.MainViewModel;

import javax.inject.Inject;

public class ListPostsFragment extends RootFragment implements OnPhotoListener, SwipeRefreshLayout.OnRefreshListener {

    private static ListPostsFragment instance;
    public static ListPostsFragment getInstance() {
        if (instance == null)
            instance = new ListPostsFragment();
        return instance;
    }
    private ListPostsFragment()
    {

    }

    MainFragmentListViewBinding bindable;

    private SwipeRefreshLayout mSwipeLayout;
    private RecyclerView mRecyclerView;
    private ProgressBar mProgressBar;
    @Inject
    MainViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        bindable = DataBindingUtil.inflate(
                inflater, R.layout.main_fragment_list_view, container, false);

        mSwipeLayout = bindable.slPhotos;
        mRecyclerView = bindable.rvPhotos;
        mProgressBar = bindable.progressBar;

        mSwipeLayout.setOnRefreshListener(this);
        //viewModel = MainViewModel.getInstance();
        viewModel.renderPostsList(this, mRecyclerView, true);

        viewModel.setUpLoadMoreListener(mRecyclerView);
        viewModel.subscribeForData(mProgressBar);

        bindable.setModel(viewModel);
        /////////////httpGetPosts();

        return bindable.getRoot();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        viewModel.destroyCompositeDisposable();
    }

    @Override
    public void onPhotoClick(int position) {
        viewModel.getIdOfSelectedMedia(position);
        if (viewModel.isPostInfoPage()) return;
        viewModel.setPostInfoPage(true);
        SinglePostFragment fragment = SinglePostFragment.getInstance();
        FragmentTransaction fragmentTransaction = viewModel.getFragmentManager()
                .beginTransaction().addToBackStack(null);
        fragmentTransaction.add(R.id.content_container, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onRefresh() {
        viewModel.setLastLoadedPage(1);
        viewModel.subscribeForData(mProgressBar);
        mSwipeLayout.setRefreshing(false);
    }
}
