package com.rush.imgurapp.ui.fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rush.imgurapp.R;
import com.rush.imgurapp.databinding.MainFragmentSingleViewBinding;
import com.rush.imgurapp.root.RootFragment;
import com.rush.imgurapp.viewmodels.MainViewModel;

import javax.inject.Inject;

public class SinglePostFragment extends RootFragment {

    private static SinglePostFragment instance;
    public static SinglePostFragment getInstance() {
        if (instance == null)
            instance = new SinglePostFragment();
        return instance;
    }

    private SinglePostFragment()
    {

    }

    MainFragmentSingleViewBinding bindable;
    RecyclerView mRecyclerView;
    @Inject
    MainViewModel viewModel;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater,
                             @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        bindable = DataBindingUtil.inflate(inflater, R.layout.main_fragment_single_view, container, false);

        //viewModel = MainViewModel.getInstance();
        mRecyclerView = bindable.rvComments;

        viewModel.getMediaData(bindable, mRecyclerView);

        return bindable.getRoot();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        viewModel.destroyDisposable();
    }
}
