package com.rush.imgurapp.di.modules;

import com.rush.imgurapp.ui.fragments.ListPostsFragment;
import com.rush.imgurapp.ui.fragments.SinglePostFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class MainFragmentBuildersModule {
    @ContributesAndroidInjector
    abstract ListPostsFragment contributesListPostsFragment();

    @ContributesAndroidInjector
    abstract SinglePostFragment contributesSinglePostFragment();
}
