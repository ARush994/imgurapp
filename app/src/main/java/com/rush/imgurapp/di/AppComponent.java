package com.rush.imgurapp.di;

import android.app.Application;

import com.rush.imgurapp.App;
import com.rush.imgurapp.di.modules.ActivityBuildersModule;
import com.rush.imgurapp.di.modules.EntireAppLifeModule;

import dagger.BindsInstance;
import dagger.Component;
import dagger.android.AndroidInjector;
import dagger.android.support.AndroidSupportInjectionModule;

@Component(
        modules = {
                AndroidSupportInjectionModule.class,
                ActivityBuildersModule.class,
                EntireAppLifeModule.class,
        })
public interface AppComponent extends AndroidInjector<App> {

    @Component.Builder
    interface Builder{

        @BindsInstance
        Builder application(Application application);

        AppComponent build();
    }
}
