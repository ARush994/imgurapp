package com.rush.imgurapp.di.modules;

import com.rush.imgurapp.data.network.ImgurStreams;
import com.rush.imgurapp.viewmodels.MainViewModel;

import dagger.Module;
import dagger.Provides;

@Module
public class EntireAppLifeModule {
    @Provides
    static MainViewModel getViewModel(){
        return MainViewModel.getInstance();
    }

    @Provides
    static ImgurStreams getImgurStreams(){
        return new ImgurStreams();
    }
}
