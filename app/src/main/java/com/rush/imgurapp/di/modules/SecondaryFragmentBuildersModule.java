package com.rush.imgurapp.di.modules;

import com.rush.imgurapp.ui.fragments.nestedfragments.ImageFragment;
import com.rush.imgurapp.ui.fragments.nestedfragments.VideoFragment;

import dagger.Module;
import dagger.android.ContributesAndroidInjector;

@Module
public abstract class SecondaryFragmentBuildersModule {
    @ContributesAndroidInjector
    abstract ImageFragment contributesImageFragment();

    @ContributesAndroidInjector
    abstract VideoFragment contributesVideoFragment();
}
