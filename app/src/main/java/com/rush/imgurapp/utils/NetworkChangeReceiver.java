package com.rush.imgurapp.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.google.android.material.snackbar.Snackbar;
import com.rush.imgurapp.data.Constants;
import com.rush.imgurapp.databinding.MainActivityViewBinding;
import com.rush.imgurapp.viewmodels.MainViewModel;

public class NetworkChangeReceiver extends BroadcastReceiver {
    Context mContext;
    MainActivityViewBinding bindable;
    public void setBindable(MainActivityViewBinding bindable) {
        this.bindable = bindable;
    }

    @Override
    public void onReceive(Context context, Intent intent) {
        mContext = context;
        String status = NetworkUtil.getConnectivityStatusString(context);

        Log.e("Receiver ", "" + status);

        if (status.equals(Constants.NOT_CONNECT)) {
            Log.e("Receiver ", "not connection");// your code when internet lost
            MainViewModel.getInstance().setShowCheckConnection(true);
            Snackbar.make(bindable.getRoot(), "Internet unavailable", Snackbar.LENGTH_SHORT).show();
        } else {
            Log.e("Receiver ", "connected to internet");//your code when internet connection come back
            MainViewModel.getInstance().setShowCheckConnection(false);
            Snackbar.make(bindable.getRoot(), "Internet available", Snackbar.LENGTH_SHORT).show();
            MainViewModel.getInstance().openingMedia();
        }
    }
}
