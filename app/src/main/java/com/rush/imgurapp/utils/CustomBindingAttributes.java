package com.rush.imgurapp.utils;

import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;

import androidx.databinding.BindingAdapter;

import com.rush.imgurapp.App;
import com.rush.imgurapp.R;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

public class CustomBindingAttributes {
    @BindingAdapter(value = {"imageId"}, requireAll = false)
    public static void setImageId(final ImageView imageView, String imageId) {
        String imageUrl = String.format("https://i.imgur.com/%s.jpg", imageId);

        Picasso.with(App.getInstance().getApplicationContext()).load(imageUrl).placeholder(R.drawable.placeholder).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
    }

    @BindingAdapter(value = {"imageUrl"}, requireAll = false)
    public static void setImageUrl(final ImageView imageView, String imageUrl) {

        Picasso.with(App.getInstance().getApplicationContext()).load(imageUrl).placeholder(R.drawable.placeholder).into(new Target() {
            @Override
            public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                imageView.setImageBitmap(bitmap);
            }

            @Override
            public void onBitmapFailed(Drawable errorDrawable) {

            }

            @Override
            public void onPrepareLoad(Drawable placeHolderDrawable) {

            }
        });
    }
}
