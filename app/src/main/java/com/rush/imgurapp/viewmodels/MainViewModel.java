package com.rush.imgurapp.viewmodels;

import android.content.Context;
import android.graphics.Rect;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import androidx.databinding.BaseObservable;
import androidx.databinding.Bindable;
import androidx.databinding.library.baseAdapters.BR;
import androidx.fragment.app.FragmentManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.recyclerview.widget.StaggeredGridLayoutManager;

import com.google.android.exoplayer2.ExoPlayerFactory;
import com.google.android.exoplayer2.SimpleExoPlayer;
import com.google.android.exoplayer2.extractor.DefaultExtractorsFactory;
import com.google.android.exoplayer2.extractor.ExtractorsFactory;
import com.google.android.exoplayer2.source.ExtractorMediaSource;
import com.google.android.exoplayer2.source.LoopingMediaSource;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.trackselection.AdaptiveTrackSelection;
import com.google.android.exoplayer2.trackselection.DefaultTrackSelector;
import com.google.android.exoplayer2.trackselection.TrackSelection;
import com.google.android.exoplayer2.trackselection.TrackSelector;
import com.google.android.exoplayer2.upstream.BandwidthMeter;
import com.google.android.exoplayer2.upstream.DataSource;
import com.google.android.exoplayer2.upstream.DefaultBandwidthMeter;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;
import com.google.android.exoplayer2.util.Util;
import com.rush.imgurapp.App;
import com.rush.imgurapp.R;
import com.rush.imgurapp.data.network.NetworkCalls;
import com.rush.imgurapp.ui.fragments.ListPostsFragment;
import com.rush.imgurapp.ui.fragments.SinglePostFragment;
import com.rush.imgurapp.ui.fragments.nestedfragments.ImageFragment;
import com.rush.imgurapp.ui.fragments.nestedfragments.VideoFragment;
import com.rush.imgurapp.adapters.CommentsRecyclerAdapter;
import com.rush.imgurapp.adapters.PhotosRecyclerAdapter;
import com.rush.imgurapp.adapters.listeners.OnPhotoListener;
import com.rush.imgurapp.databinding.MainFragmentSingleViewBinding;
import com.rush.imgurapp.models.CommentModel;
import com.rush.imgurapp.models.PostInnerModel;
import com.rush.imgurapp.models.PostModel;
import com.rush.imgurapp.models.ResponseWithCommentModel;
import com.rush.imgurapp.data.network.ImgurStreams;

import java.util.ArrayList;
import java.util.List;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.disposables.Disposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.processors.PublishProcessor;
import io.reactivex.schedulers.Schedulers;

public class MainViewModel extends BaseObservable {

    private static MainViewModel instance;
    public static MainViewModel getInstance() {
        if (instance == null) {
            instance = new MainViewModel();
        }
        return instance;
    }

    private MainViewModel()
    {
        postsList = new ArrayList<>();
        applicationContext = App.getInstance().getBaseContext();
        lastLoadedPage = 1;
        postInfoPage = false;
    }

    private FragmentManager fragmentManager;
    public FragmentManager getFragmentManager() {
        return fragmentManager;
    }
    public void setFragmentManager(FragmentManager fragmentManager) {
        this.fragmentManager = fragmentManager;
    }


    private boolean postInfoPage;
    public boolean isPostInfoPage() {
        return postInfoPage;
    }
    public void setPostInfoPage(boolean isPostInfoPage) {
        this.postInfoPage = isPostInfoPage;
    }

    private Context applicationContext;
    public Context getApplicationContext() {
        return applicationContext;
    }

    private List<PostModel> postsList;
    public List<PostModel> getPostsList() {
        return postsList;
    }
    public void setPostsList(List<PostModel> postsList) {
        this.postsList = postsList;
    }
    public void addToPostsList(List<PostModel> postsList) {
        this.postsList.addAll(postsList);
    }

    private int lastLoadedPage;
    public int getLastLoadedPage() {
        return lastLoadedPage;
    }
    public void setLastLoadedPage(int lastLoadedPage) {
        this.lastLoadedPage = lastLoadedPage;
    }

    private String currentPostId;
    public String getCurrentPostId() {
        return currentPostId;
    }
    public void setCurrentPostId(String currentPostId) {
        this.currentPostId = currentPostId;
    }

    private PostInnerModel postInner;
    public PostInnerModel getPostInner() {
        return postInner;
    }
    public void setPostInner(PostInnerModel postInner) {
        this.postInner = postInner;
    }

    private List<CommentModel> commentsList;
    public List<CommentModel> getCommentsList() {
        return commentsList;
    }
    public void setCommentsList(List<CommentModel> commentsList) {
        this.commentsList = commentsList;
    }

    private SimpleExoPlayer player;
    public SimpleExoPlayer getPlayer() {
        return player;
    }
    public void setPlayer(SimpleExoPlayer player) {
        this.player = player;
    }

    private PhotosRecyclerAdapter photosAdapter;
    public PhotosRecyclerAdapter getPhotosAdapter() {
        return photosAdapter;
    }
    public void setPhotosAdapter(PhotosRecyclerAdapter photosAdapter) {
        this.photosAdapter = photosAdapter;
    }

    private StaggeredGridLayoutManager staggeredManager;
    public StaggeredGridLayoutManager getStaggeredManager() {
        return staggeredManager;
    }
    public void setStaggeredManager(StaggeredGridLayoutManager staggeredManager) {
        this.staggeredManager = staggeredManager;
    }

    private ImgurStreams imgurStreams;
    public ImgurStreams getImgurStreams() {
        return imgurStreams;
    }
    public void setImgurStreams(ImgurStreams imgurStreams) {
        this.imgurStreams = imgurStreams;
    }

    private boolean showCheckConnection = false;
    @Bindable
    public boolean isShowCheckConnection() {
        return showCheckConnection;
    }
    public void setShowCheckConnection(boolean showCheckConnection) {
        this.showCheckConnection = showCheckConnection;
        notifyPropertyChanged(BR.showCheckConnection);
    }

    public boolean connectionChecked = false;
    public boolean isConnectionChecked() {
        return connectionChecked;
    }
    public void setConnectionChecked(boolean connectionChecked) {
        this.connectionChecked = connectionChecked;
    }

    public void getIdOfSelectedMedia(int i)
    {
        setCurrentPostId(postsList.get(i).getId());
    }

    //region Starting MainActivity

    private boolean checkingConnection() {
        ConnectivityManager cm =
                (ConnectivityManager)applicationContext.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();
        boolean isConnected = false;
        if (activeNetwork != null)
            isConnected = activeNetwork.isConnectedOrConnecting();
        if (!isConnected)
        {
            setShowCheckConnection(true);
            Toast.makeText(applicationContext, "No internet connection", Toast.LENGTH_SHORT).show();
        } else {
            setShowCheckConnection(false);
            Toast.makeText(applicationContext, "Internet connection", Toast.LENGTH_SHORT).show();
            openingMedia();
        }
        return isConnected;
    }

    public void openingMedia() {
        setImgurStreams(imgurStreams);
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            SinglePostFragment fragment = SinglePostFragment.getInstance();
            getFragmentManager().beginTransaction()
                    .replace(R.id.content_container, fragment)
                    .commit();
        } else {
            ListPostsFragment fragment = ListPostsFragment.getInstance();
            getFragmentManager().beginTransaction()
                    .replace(R.id.content_container, fragment)
                    .commit();
        }
    }

    public void selectFragment(){
        if (getPostInner().getMp4() == null) {
            ImageFragment fragment = new ImageFragment();
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment_frame, fragment)
                    .commit();
            getPostInner().setMp4("https://i.imgur.com/bEPlmuP.mp4");
        } else {
            VideoFragment fragment = new VideoFragment();
            fragmentManager.beginTransaction()
                    .replace(R.id.fragment_frame, fragment)
                    .commit();
        }
    }
    //endregion

    //region Render lists to RecyclerViews
    public void renderPostsList(OnPhotoListener listener, RecyclerView rv, boolean isEmpty)
    {
        if (isEmpty)
            setPhotosAdapter(new PhotosRecyclerAdapter(listener));
        else
            setPhotosAdapter(new PhotosRecyclerAdapter(listener, getPostsList()));
        setStaggeredManager(new StaggeredGridLayoutManager(2,StaggeredGridLayoutManager.VERTICAL));
        rv.setLayoutManager(getStaggeredManager());

        rv.setHasFixedSize(true);
        rv.setItemViewCacheSize(0);
        //rv.setDrawingCacheEnabled(true);
        rv.setDrawingCacheQuality(View.DRAWING_CACHE_QUALITY_LOW);

        rv.setAdapter(getPhotosAdapter()); // Установка адаптера для списка
        rv.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                outRect.bottom = 16; // Gap of 16px
            }
        });
    }

    public void renderCommentsList(RecyclerView rv)
    {
        CommentsRecyclerAdapter mAdapter =
                new CommentsRecyclerAdapter(getCommentsList());
        LinearLayoutManager manager =
                new LinearLayoutManager(getApplicationContext(), RecyclerView.VERTICAL, false);
        rv.setLayoutManager(manager);
        rv.setHasFixedSize(true);

        rv.setAdapter(mAdapter); // Установка адаптера для списка

        rv.addItemDecoration(new RecyclerView.ItemDecoration() {
            @Override
            public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
                outRect.bottom = 16; // Gap of 16px
            }
        });
    }
    //endregion

    //region ExoPlayer
    public void initializePlayer() {
        // Create a default TrackSelector
        BandwidthMeter bandwidthMeter = new DefaultBandwidthMeter();
        TrackSelection.Factory videoTrackSelectionFactory =
                new AdaptiveTrackSelection.Factory(bandwidthMeter);
        //Initialize the player
        TrackSelector trackSelector =
                new DefaultTrackSelector(videoTrackSelectionFactory);
        player = ExoPlayerFactory.newSimpleInstance(getApplicationContext(), trackSelector);
    }

    public void setPlayerUri(){
        // Produces DataSource instances through which media data is loaded.
        DataSource.Factory dataSourceFactory =
                new DefaultDataSourceFactory(getApplicationContext(), Util.getUserAgent(getApplicationContext(),
                        getApplicationContext().getString(R.string.app_name)));

        // Produces Extractor instances for parsing the media data.
        ExtractorsFactory extractorsFactory = new DefaultExtractorsFactory();

        // This is the MediaSource representing the media to be played.
        Uri videoUri = Uri.parse(getPostInner().getMp4());
        MediaSource videoSource = new ExtractorMediaSource(videoUri,
                dataSourceFactory, extractorsFactory, null, null);

        // Prepare the player with the source.
        LoopingMediaSource loopingSource = new LoopingMediaSource(videoSource);
        player.prepare(loopingSource);
        playVideo(true);
    }

    public void playVideo(boolean isPlay)
    {
        player.setPlayWhenReady(isPlay);
    }

    public void releasePlayer() {
        if (player != null) {
            player.release();
            player = null;
        }
    }
    //endregion

    //region SingleFragmet: HTTP Call (RxJAVA)
    public void getMediaData(MainFragmentSingleViewBinding bindable, RecyclerView mRecyclerView)
    {
        NetworkCalls.getInstance().httpGettingMediaModel(this, bindable, mRecyclerView);
    }

    public void destroyDisposable()
    {
        NetworkCalls.getInstance().destroyingDisposable();
    }
    //endregion

    //region ListFragmet: HTTP (RxJAVA)
    // For paging
    private int lastVisibleItem, totalItemCount;

    public boolean loading = false;
    public boolean getLoading()
    {
        return loading;
    }
    public void setLoading(boolean loading) {
        this.loading = loading;
    }

    private PublishProcessor<Integer> paginator = PublishProcessor.create();
    private final int VISIBLE_THRESHOLD = 2;

    public void setUpLoadMoreListener(RecyclerView mRecyclerView) {
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(RecyclerView recyclerView,
                                   int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                StaggeredGridLayoutManager layoutManager = getStaggeredManager();
                int item[] = new int[5];
                totalItemCount = layoutManager.getItemCount();
                item = layoutManager.findLastVisibleItemPositions(item);
                lastVisibleItem = item[0];
                if (!loading
                        && totalItemCount <= (lastVisibleItem + VISIBLE_THRESHOLD)) {
                    setLastLoadedPage(getLastLoadedPage() + 1);
                    NetworkCalls.getInstance().getPaginator().onNext(getLastLoadedPage());
                    loading = true;
                }
            }
        });
    }

    public void subscribeForData(ProgressBar mProgressBar) {
        NetworkCalls.getInstance().subscribeingForData(mProgressBar, this);
    }

    public void destroyCompositeDisposable(){
        NetworkCalls.getInstance().destroyingCompositeDisposable();
    }

    //endregion

    //region SingleFragment: downloadMedia
    public void downloadMedia(){

    }

    //Method to Check If SD Card is mounted or not
    public boolean isSDCardPresent() {
        if (Environment.getExternalStorageState().equals(

                Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }

    public void mediaDownloading(Context context)
    {
        new NetworkCalls.MediaDownloading(context).execute(getPostInner().getLink());
    }
    //endregion
}
