package com.rush.imgurapp.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rush.imgurapp.R;
import com.rush.imgurapp.adapters.listeners.OnPhotoListener;
import com.rush.imgurapp.databinding.FragmentRecyclerCommentItemBinding;
import com.rush.imgurapp.models.CommentModel;

import java.util.List;

public class CommentsRecyclerAdapter extends RecyclerView.Adapter<CommentsRecyclerAdapter.SingleItemHolder> {

    private List<CommentModel> list;

    public CommentsRecyclerAdapter(List<CommentModel> list) {
        this.list = list;
    }

    public class SingleItemHolder extends RecyclerView.ViewHolder {
        private FragmentRecyclerCommentItemBinding bindable;

        public SingleItemHolder(@NonNull View v) {
            super(v);
            bindable = DataBindingUtil.bind(v);
        }

        public FragmentRecyclerCommentItemBinding getBinding() {
            return bindable;
        }
    }

    @NonNull
    @Override
    public SingleItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.fragment_recycler_comment_item, parent, false);
        return new SingleItemHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull SingleItemHolder holder, int position) {
        CommentModel item = list.get(position);
        switch (item.getPlatform())
        {
            case "iphone":
                item.setApple(true);
                break;
            case "android":
                item.setAndroid(true);
                break;
            case "desktop":
                item.setDesktop(true);
                break;
            case "api":
                item.setApi(true);
                break;
                default:
                    item.setText(true);
                    break;
        }
        holder.getBinding().setModel(item);
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }
}
