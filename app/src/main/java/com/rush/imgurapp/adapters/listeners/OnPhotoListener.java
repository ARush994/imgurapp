package com.rush.imgurapp.adapters.listeners;

public interface OnPhotoListener
{
    void onPhotoClick(int position);
}
