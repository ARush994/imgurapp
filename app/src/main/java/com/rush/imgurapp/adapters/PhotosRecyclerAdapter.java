package com.rush.imgurapp.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.rush.imgurapp.R;
import com.rush.imgurapp.adapters.listeners.OnPhotoListener;
import com.rush.imgurapp.databinding.MainRecyclerPhotoItemBinding;
import com.rush.imgurapp.models.PostModel;

import java.util.ArrayList;
import java.util.List;

public class PhotosRecyclerAdapter extends RecyclerView.Adapter<PhotosRecyclerAdapter.SingleItemHolder> {

    private List<PostModel> list = new ArrayList<>();
    private OnPhotoListener mOnPhotoListener;

    public PhotosRecyclerAdapter(OnPhotoListener onPhotoListener, List<PostModel> list) {
        this.mOnPhotoListener = onPhotoListener;
        this.list = list;
    }

    public PhotosRecyclerAdapter(OnPhotoListener onPhotoListener)
    {
        this.mOnPhotoListener = onPhotoListener;
    }

    public void addItems(List<PostModel> items) {
        this.list.addAll(items);
    }

    public class SingleItemHolder extends RecyclerView.ViewHolder {
        private MainRecyclerPhotoItemBinding bindable;
        private OnPhotoListener onPhotoListener;

        public SingleItemHolder(@NonNull View v, OnPhotoListener onPhotoListener) {
            super(v);
            this.onPhotoListener = onPhotoListener;
            bindable = DataBindingUtil.bind(v);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onPhotoListener.onPhotoClick(getAdapterPosition());
                }
            });
        }

        public MainRecyclerPhotoItemBinding getBinding() {
            return bindable;
        }
    }

    @NonNull
    @Override
    public SingleItemHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.main_recycler_photo_item, parent, false);
        return new SingleItemHolder(v, mOnPhotoListener);
    }

    @Override
    public void onBindViewHolder(@NonNull final SingleItemHolder holder, int position) {
        PostModel item = list.get(position);
        if(item.getIsAlbum()) {
            item.setId(item.getCover());
        }
        holder.getBinding().setModel(item);
        holder.getBinding().executePendingBindings();
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getItemViewType(int position) {
        return position;
    }
}
